﻿using ChildrenGame.API;
using Moq;
using NUnit.Framework;

namespace ChildrenGame.Tests
{
    /// <summary>
    /// APIController tests
    /// </summary>
    [TestFixture]
    public class ChildrenGameControllerTests
    {
        /// <summary>
        /// Test run method returns the correct results
        /// </summary>
        [Test]
        public void Run_completes_correctly()
        {
            // setup
            ChildrenGameController childrenGameController = new ChildrenGameController();
            RequestGameJsonResponse requestGameJsonResponse = new RequestGameJsonResponse()
            {
                id = 81381,
                children_count = 3,
                eliminate_each = 1
            };
            Mock<APIController> apiController = new Mock<APIController>();
            apiController.Setup(s => s.RetrieveGameParameters()).ReturnsAsync(requestGameJsonResponse);

            // act
            childrenGameController.Run(apiController.Object);

            // verify
            apiController.VerifyAll();
        }

        /// <summary>
        /// Test run method when parameter is null
        /// </summary>
        [Test]
        public void Run_game_parameters_are_null()
        {
            // setup
            ChildrenGameController childrenGameController = new ChildrenGameController();
            RequestGameJsonResponse requestGameJsonResponse = null;
            Mock<APIController> apiController = new Mock<APIController>();
            apiController.Setup(s => s.RetrieveGameParameters()).ReturnsAsync(requestGameJsonResponse);

            // act
            childrenGameController.Run(apiController.Object);

            // verify
            apiController.Verify(s => s.RetrieveGameParameters(), Times.Once);
            apiController.Verify(s => s.SendGameResults(It.IsAny<GameResultsJsonResponse>()), Times.Never);
        }

        /// <summary>
        /// Test run method when when children count is zero
        /// </summary>
        [Test]
        public void Run_children_count_and_eliminate_each_are_zero()
        {
            // setup
            ChildrenGameController childrenGameController = new ChildrenGameController();
            RequestGameJsonResponse requestGameJsonResponse = new RequestGameJsonResponse()
            {
                id = 11111,
                children_count = 0,
                eliminate_each = 0
            };
            Mock<APIController> apiController = new Mock<APIController>();
            apiController.Setup(s => s.RetrieveGameParameters()).ReturnsAsync(requestGameJsonResponse);

            // act
            childrenGameController.Run(apiController.Object);

            // verify
            apiController.Verify(s => s.RetrieveGameParameters(), Times.Once);
            apiController.Verify(s => s.SendGameResults(It.IsAny<GameResultsJsonResponse>()), Times.Never);
        }
    }
}