﻿using ChildrenGame.API;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChildrenGame.Tests
{
    /// <summary>
    /// Children tests
    /// </summary>
    [TestFixture]
    public class ChildrenTests
    {
        /// <summary>
        /// Test instantiation of Children class
        /// Adds one child
        /// </summary>
        [Test]
        public void AddChild_adds_one_child()
        {
            // setup
            Children children = new Children(1);

            // verify
            Assert.AreEqual(1, children.Count);
            Assert.AreEqual(1, children.First.Id);
            Assert.AreEqual(children.First.Id, children.Last.Id);
        }

        /// <summary>
        /// Test instantiation of Children class
        /// Adds more than one child child
        /// </summary>
        [Test]
        public void AddChild_adds_many_children()
        {
            // setup
            Children children = new Children(10);

            // verify
            Assert.AreEqual(10, children.Count);
            Assert.AreEqual(1, children.First.Id);
            Assert.AreEqual(3, children.First.Next.Next.Id);
            Assert.AreEqual(10, children.Last.Id);
        }

        /// <summary>
        /// Test AddChild for invalid case
        /// </summary>
        /// <param name="id">The id of child</param>
        [Test]
        [TestCase(5)]
        [TestCase(1)]
        public void AddChild_add_invalid_id(int id)
        {
            // setup
            Children children = new Children(5);

            // act
            children.AddChild(id);

            //verify
            Assert.AreEqual(5, children.Count);
            Assert.AreEqual(5, children.Last.Id);
        }

        /// <summary>
        /// Test AddChild for invalid cases
        /// </summary>
        [Test]
        [TestCase(0)]
        [TestCase(-3)]
        public void AddChild_invalid_children_count(int childrenCount)
        {
            // setup
            Children children = new Children(childrenCount);

            // verify
            Assert.AreEqual(null, children.First);
        }

        /// <summary>
        /// Test RemoveChild method.
        /// Test case for removing a child in the middle
        /// </summary>
        [Test]
        public void RemoveChild_removes_child_in_front_and_rearranges_correctly()
        {
            // setup
            Children children = new Children(5);

            // act
            Child removedChild = children.RemoveChild(0); // Element 1 => index is 0

            // verify
            Assert.AreNotEqual(null, removedChild);
            Assert.AreEqual(4, children.Count);
            Assert.AreEqual(2, children.First.Id);
            Assert.AreEqual(5, children.Last.Id);
        }

        /// <summary>
        /// Test RemoveChild method.
        /// Test case for removing a child at the front
        /// </summary>
        [Test]
        public void RemoveChild_removes_child_in_middle_and_rearranges_correctly()
        {
            // setup
            Children children = new Children(5);

            // act
            Child removedChild = children.RemoveChild(2); // Element 3 => index is 2

            // verify
            Assert.AreNotEqual(null, removedChild);
            Assert.AreEqual(4, children.Count);
            Assert.AreEqual(4, children.First.Id);
            Assert.AreEqual(2, children.Last.Id);
        }

        /// <summary>
        /// Test RemoveChild method.
        /// Test case for removing a child at the end
        /// </summary>
        [Test]
        public void RemoveChild_removes_child_in_end_and_rearranges_correctly()
        {
            // setup
            Children children = new Children(5);

            // act
            Child removedChild = children.RemoveChild(4); // Element 5 => index is 4

            // verify
            Assert.AreNotEqual(null, removedChild);
            Assert.AreEqual(4, children.Count);
            Assert.AreEqual(1, children.First.Id);
            Assert.AreEqual(4, children.Last.Id);
        }

        /// <summary>
        /// Test RemoveChild method for invalid index
        /// </summary>
        [Test]
        [TestCase(-1)]
        [TestCase(10)]
        public void RemoveChild_invalid_case(int index)
        {
            // setup
            Children children = new Children(5);

            // act
            Child removedChild = children.RemoveChild(index);

            // verify
            Assert.AreEqual(null, removedChild);
            Assert.AreEqual(5, children.Count);
            Assert.AreEqual(1, children.First.Id);
            Assert.AreEqual(5, children.Last.Id);
        }

        /// <summary>
        /// Test PerformElimination method for valid cases for childrenCount and eliminateEach
        /// The list of ints are passed using a string. It is converted to a list of ints using
        /// ConvertStringToListOfInts(string orderOfElimination)
        /// </summary>
        /// <param name="childrenCount">The number of children</param>
        /// <param name="eliminateEach">The child to eliminate</param>
        /// <param name="winner">The winning child</param>
        /// <param name="orderOfElimination">The children that are eliminated</param>
        [Test]
        [TestCase(10, 2, 5, "2, 4, 6, 8, 10, 3, 7, 1, 9")]
        [TestCase(10, 20, 1, "10, 2, 6, 4, 7, 5, 3, 9, 8")]
        [TestCase(10, 69, 6, "9, 5, 1, 8, 3, 10, 2, 7, 4")]
        [TestCase(10, 1, 10, "1, 2, 3, 4, 5, 6, 7, 8, 9")]
        [TestCase(10, 10, 8, "10, 1, 3, 6, 2, 9, 5, 7, 4")]
        [TestCase(1, 1, 1, "")]
        [TestCase(1, 20, 1, "")]
        public void PerformElimination_performs_elimination_correctly(int childrenCount, int eliminateEach, int winner, string orderOfElimination)
        {
            // setup
            Children children = new Children(childrenCount);

            // act
            GameResultsJsonResponse gameResultsJsonResponse = children.PerformElimination(1, eliminateEach);

            // verify
            Assert.AreNotEqual(null, gameResultsJsonResponse);
            Assert.AreEqual(winner, gameResultsJsonResponse.last_child);
            CollectionAssert.AreEqual(ConvertStringToListOfInts(orderOfElimination), gameResultsJsonResponse.order_of_elimination);
        }

        /// <summary>
        /// Test PerformElimination method for invalid values of children count
        /// </summary>
        /// <param name="childrenCount">The number of children</param>
        [Test]
        [TestCase(0)]
        [TestCase(-2)]
        public void PerformElimination_returns_null_when_children_count_is_invalid(int childrenCount)
        {
            // setup
            Children children = new Children(childrenCount);

            // act
            GameResultsJsonResponse gameResultsJsonResponse = children.PerformElimination(1, 5);

            // verify
            Assert.AreEqual(null, gameResultsJsonResponse);
        }

        /// <summary>
        /// Test PerformElimination method for invalid values of its eliminateEach parameter
        /// </summary>
        /// <param name="eliminateEach">The child to eliminate</param>
        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void PerformElimination_returns_null_when_eliminate_each_is_invalid(int eliminateEach)
        {
            // setup
            Children children = new Children(10);

            // act
            GameResultsJsonResponse gameResultsJsonResponse = children.PerformElimination(1, eliminateEach);

            // verify
            Assert.AreEqual(null, gameResultsJsonResponse);
        }

        /// <summary>
        /// Test Reset method
        /// </summary>
        [Test]
        public void Reset_change_property_values_correctly()
        {
            // setup
            Children children = new Children(5);

            // act
            children.PerformElimination(1, 1);

            // verify
            Assert.AreEqual(null, children.First);
            Assert.AreEqual(null, children.Last);
            Assert.AreEqual(0, children.Count);
        }

        /// <summary>
        /// Convert a string that contains a list of comma seperated ints into  list of ints
        /// This is used by PerformElimination_performs_elimination_correctly(int childrenCount, int eliminateEach, int winner, string orderOfElimination)
        /// </summary>
        /// <param name="orderOfElimination"></param>
        /// <returns>A list of ints</returns>
        private List<int> ConvertStringToListOfInts(string orderOfElimination)
        {
            List<int> expectedOrderOfElimination = new List<int>();
            if (orderOfElimination == string.Empty)
            {
                return expectedOrderOfElimination;
            }

            string[] orderOfEliminationInStringArray = orderOfElimination.Split(',');

            expectedOrderOfElimination.AddRange(orderOfEliminationInStringArray.Select(eliminatedIds => Convert.ToInt32(eliminatedIds)));

            return expectedOrderOfElimination;
        }
    }
}