﻿using ChildrenGame.API;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace ChildrenGame.Tests
{
    /// <summary>
    /// APIController tests
    /// </summary>
    [TestFixture]
    public class APIControllerTests
    {
        /// <summary>
        /// Test RetrieveGameParameters method. It should return a RequestGameJSONResponse object
        /// </summary>
        [Test]
        public void RetrieveGameParameters_retrieves_the_game_parameters_correctly()
        {
            // setup
            APIController apiController = new APIController();

            // act
            Task<RequestGameJsonResponse> response = apiController.RetrieveGameParameters();
            RequestGameJsonResponse requestGameJsonResponse = response.Result;

            // verify
            Assert.AreNotEqual(null, requestGameJsonResponse);
            Assert.AreNotEqual(0, requestGameJsonResponse.id);
        }

        /// <summary>
        /// Test RetrieveGameParameters mehod for exception
        /// </summary>
        [Test]
        public void RetrieveGameParameters_catches_internal_exception()
        {
            // setup
            APIController apiController = new APIController
            {
                httpClient = null
            };

            // act
            Task<RequestGameJsonResponse> result = apiController.RetrieveGameParameters();

            // verify
            Assert.AreEqual(null, result.Result);
        }

        /// <summary>
        /// Test valid case of RetrieveGameParameters method
        /// </summary>
        [Test]
        public void SendGameResults_posts_valid_results_successfully()
        {
            // setup
            APIController apiController = new APIController();
            GameResultsJsonResponse gameResultsJsonResponse = new GameResultsJsonResponse()
            {
                id = 81381,
                last_child = 3,
                order_of_elimination = new List<int>() { 1, 2 }
            };

            // act
            Task<HttpStatusCode> response = apiController.SendGameResults(gameResultsJsonResponse);
            HttpStatusCode status = response.Result;

            // verify
            Assert.AreEqual(HttpStatusCode.OK, status);
        }

        /// <summary>
        /// Test invalid case of RetrieveGameParameters method
        /// </summary>
        [Test]
        public void SendGameResults_posts_invalid_results()
        {
            // setup
            APIController apiController = new APIController();
            GameResultsJsonResponse gameResultsJsonResponse = new GameResultsJsonResponse();

            // act
            Task<HttpStatusCode> response = apiController.SendGameResults(gameResultsJsonResponse);
            HttpStatusCode status = response.Result;

            // verify
            Assert.AreNotEqual(HttpStatusCode.OK, status);
        }

        /// <summary>
        /// Test SendGameResults method for exception
        /// </summary>
        [Test]
        public void SendGameResults_catches_internal_exception()
        {
            // setup
            APIController apiController = new APIController
            {
                httpClient = null
            };
            GameResultsJsonResponse gameResultsJsonResponse = new GameResultsJsonResponse();

            // act
            Task<HttpStatusCode> result = apiController.SendGameResults(gameResultsJsonResponse);

            // verify
            Assert.AreEqual(HttpStatusCode.BadRequest, result.Result);
        }
    }
}