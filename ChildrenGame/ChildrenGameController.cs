﻿using ChildrenGame.API;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace ChildrenGame
{
    /// <summary>
    /// The main controller of the children game
    /// </summary>
    public class ChildrenGameController
    {
        /// <summary>
        /// Contains the main flow of the application (excluding api calls)
        /// </summary>
        /// <param name="apiController">The api controller object</param>
        public void Run(APIController apiController)
        {
            // Get the game parameters from API
            Task<RequestGameJsonResponse> response = apiController.RetrieveGameParameters();
            RequestGameJsonResponse requestGameJsonResponse = response.Result;

            // If game parameters are not retrieved
            if (requestGameJsonResponse == null)
            {
                Console.WriteLine("No game parameters were retrieved.");
                return;
            }

            // Display game parameters on console
            Console.Write("Retrieved game parameters: ");
            Console.WriteLine(JsonConvert.SerializeObject(requestGameJsonResponse));

            int childrenCount = requestGameJsonResponse.children_count;
            int eliminateEach = requestGameJsonResponse.eliminate_each;

            if (childrenCount <= 0 || eliminateEach <= 0)
            {
                Console.WriteLine("Game parameters: children_count and eliminate_each, must be greater than zero.");
                return;
            }

            // Create the child objects for the circle with the specified number
            Children children = new Children(childrenCount);

            // Perform the elimination and get the results in a GameResultsJSONResponse object
            GameResultsJsonResponse gameResultsJsonResponse = children.PerformElimination(requestGameJsonResponse.id, eliminateEach);

            if (gameResultsJsonResponse != null)
            {
                // Display results on console
                Console.WriteLine("\nWinning child: " + gameResultsJsonResponse.last_child);
                Console.Write("Eliminated children in order: ");
                Console.WriteLine("[{0}]", String.Join(", ", gameResultsJsonResponse.order_of_elimination));

                // POST to API
                Task<HttpStatusCode> status = apiController.SendGameResults(gameResultsJsonResponse);

                // Display POST result
                Console.Write("\nGame results POST status: ");
                Console.WriteLine(status.Result);
            }
        }
    }
}