﻿using ChildrenGame.API;
using System.Collections.Generic;

namespace ChildrenGame
{
    /// <summary>
    /// Class representing children in the circle
    /// </summary>
    public class Children
    {
        /// <summary>
        /// Reference to first child
        /// </summary>
        public Child First { get; private set; }

        /// <summary>
        /// Reference to last child
        /// </summary>
        public Child Last { get; private set; }

        /// <summary>
        /// Number of children in circle
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Instantiate the class with the given number of children
        /// </summary>
        /// <param name="numberofChildren">The number of children</param>
        public Children(int numberofChildren)
        {
            for (int i = 1; i <= numberofChildren; ++i)
            {
                AddChild(i);
            }
        }

        /// <summary>
        /// Eliminate a child at a time.
        /// The result contains the winner and order of elimination
        /// </summary>
        /// <param name="gameId">The game id</param>
        /// <param name="eliminateEach">The k'th child to eliminate</param>
        /// <returns>A game results json response object</returns>
        public GameResultsJsonResponse PerformElimination(int gameId, int eliminateEach)
        {
            // Do not perform the elimination process if there isn't atleast one child
            // nor if eliminateEach is zero. (k must be greater than zero)
            if (First == null || eliminateEach <= 0)
            {
                return null;
            }

            // list to contain the eliminated child ids
            List<int> orderOfElimination = new List<int>();

            // loop until one child remains
            while (this.Count > 1)
            {
                // get the position of the child to remove.
                // this will be between 0 to (count - 1) due to the modulo operator
                int positionToRemove = eliminateEach % this.Count;
                int indexToRemove = 0;

                if (positionToRemove == 0)
                {
                    // since position is zero, we remove the last child
                    indexToRemove = this.Count - 1;
                }
                else
                {
                    // since indexes start from zero, we deduct one
                    indexToRemove = positionToRemove - 1;
                }

                // add removed child's id to list
                Child removedChild = this.RemoveChild(indexToRemove);
                orderOfElimination.Add(removedChild.Id);
            }

            // instantiate the GameResultsJSONResponse object with the results
            GameResultsJsonResponse gameResultsJsonResponse = new GameResultsJsonResponse()
            {
                id = gameId,
                last_child = First.Id,
                order_of_elimination = orderOfElimination
            };

            // Reset the values of the properties, so that the object can be re-used
            Reset();

            return gameResultsJsonResponse;
        }

        /// <summary>
        /// Add a child to the circle with the given id
        /// </summary>
        /// <param name="id">The id</param>
        public void AddChild(int id)
        {
            // Since the id is a sequential number,
            // the new id must be greater than the count
            if (id <= Count)
            {
                return;
            }

            Child newChild = new Child(id);

            // When adding first child
            if (First == null)
            {
                First = Last = newChild;
            }
            else
            {
                // When atleast one child exists,
                // add the new child as the next child of the last
                // and change the last child to the newly added one
                Last.Next = newChild;
                Last = newChild;
            }

            ++Count;
        }

        /// <summary>
        /// Remove child of the given index
        /// Index should be between 0 and (Count-1)
        /// </summary>
        /// <param name="index">The index</param>
        /// <returns>The removed Child</returns>
        public Child RemoveChild(int index)
        {
            // check if index is valid
            if (index < 0 || index >= this.Count)
            {
                return null;
            }

            Child removingChild;

            if (index == 0)
            {
                removingChild = First;
                First = First.Next;
            }
            else
            {
                int counter = 0;
                Child child = First;

                // iterate to the child immediately before
                // the child to be removed
                while ((counter < (index - 1)) && (child != null))
                {
                    child = child.Next;
                    ++counter;
                }

                removingChild = child.Next;

                // Re-arrange the children; such that the child immediately next to the
                // eliminated one becomes First, the child immediately before the eliminated one
                // becomes last and the previously First child is immediately next
                // to the previously Last child. The first two operations are not required,
                // if removing the last child
                if (removingChild.Id != Last.Id)
                {
                    Last.Next = First;
                    First = removingChild.Next;
                }

                child.Next = null;
                Last = child;
            }

            // decrement the count of children
            --Count;

            return removingChild;
        }

        /// <summary>
        /// Reset the values of the properties of this class
        /// </summary>
        public void Reset()
        {
            First = Last = null;
            Count = 0;
        }
    }
}