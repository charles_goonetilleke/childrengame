﻿namespace ChildrenGame
{
    /// <summary>
    /// Class for constants
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// URL of API
        /// </summary>
        public const string URL = "https://7annld7mde.execute-api.ap-southeast-2.amazonaws.com/main/game/";
    }
}