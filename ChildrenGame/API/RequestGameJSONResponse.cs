﻿namespace ChildrenGame.API
{
    /// <summary>
    /// Model for containing the game retrieved using GET API call
    /// </summary>
    public class RequestGameJsonResponse
    {
        /// <summary>
        /// The game id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// The number of children in the circle
        /// </summary>
        public int children_count { get; set; }

        /// <summary>
        /// The k'th child that gets eliminated
        /// </summary>
        public int eliminate_each { get; set; }
    }
}