﻿using System.Collections.Generic;

namespace ChildrenGame.API
{
    /// <summary>
    /// Model for containing data for POST API call
    /// </summary>
    public class GameResultsJsonResponse
    {
        /// <summary>
        /// Game id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// The winning child
        /// </summary>
        public int last_child { get; set; }

        /// <summary>
        /// The order of elimination
        /// </summary>
        public List<int> order_of_elimination { get; set; }
    }
}