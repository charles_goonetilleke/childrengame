﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ChildrenGame.API
{
    /// <summary>
    /// The API Controller
    /// </summary>
    public class APIController
    {
        /// <summary>
        /// The HttpClient object
        /// </summary>
        public HttpClient httpClient;

        /// <summary>
        /// Instantiate httpclient object.
        /// </summary>
        public APIController()
        {
            httpClient = new HttpClient()
            {
                BaseAddress = new Uri(Constants.URL)
            };
        }

        /// <summary>
        /// Get the game parameters
        /// </summary>
        /// <returns>A RequestGameJsonResponse object</returns>
        public virtual async Task<RequestGameJsonResponse> RetrieveGameParameters()
        {
            RequestGameJsonResponse requestGameJsonResponse = null;

            try
            {
                // Add an Accept header for JSON format.
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Data response.
                HttpResponseMessage response = await httpClient.GetAsync(string.Empty);

                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    requestGameJsonResponse = await response.Content.ReadAsAsync<RequestGameJsonResponse>();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("An error occured when trying to retrieve game parameters from API.");
            }

            return requestGameJsonResponse;
        }

        /// <summary>
        /// Post the game results
        /// </summary>
        /// <param name="gameResultsJsonResponse">A GameResultsJsonResponse object</param>
        /// <returns>The status</returns>
        public virtual async Task<HttpStatusCode> SendGameResults(GameResultsJsonResponse gameResultsJsonResponse)
        {
            try
            {
                // Add an Accept header for JSON format.
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Data response.
                HttpResponseMessage response = await httpClient.PostAsJsonAsync(gameResultsJsonResponse.id.ToString(), gameResultsJsonResponse);

                return response.StatusCode;
            }
            catch (Exception)
            {
                Console.WriteLine("An error occured when trying to send results back to API.");
                return HttpStatusCode.BadRequest;
            }
        }
    }
}