﻿namespace ChildrenGame
{
    /// <summary>
    /// Class representing a child
    /// </summary>
    public class Child
    {
        /// <summary>
        /// The next child
        /// </summary>
        public Child Next { get; set; }

        /// <summary>
        /// The id of the child
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Instantiate child object with given id
        /// </summary>
        /// <param name="id">The id</param>
        public Child(int id)
        {
            this.Id = id;
        }
    }
}