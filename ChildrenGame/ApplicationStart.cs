﻿using ChildrenGame.API;
using System;

namespace ChildrenGame
{
    /// <summary>
    /// The starting point of the application
    /// </summary>
    public class ApplicationStart
    {
        /// <summary>
        /// The main method
        /// </summary>
        /// <param name="args">The arguments</param>
        public static void Main(string[] args)
        {
            try
            {
                // Initialize controller objects and run the game
                ChildrenGameController childrenGameController = new ChildrenGameController();
                APIController apiController = new APIController();
                childrenGameController.Run(apiController);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                Console.WriteLine("Press any key to close...");
                Console.ReadLine();
            }
        }
    }
}